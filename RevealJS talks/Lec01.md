### Visual Exploration of Public Health Data

Created by <br> Ella Temprosa and Naji Younes
	
October 28, 2015

// Oh hey, these are some notes. They'll be hidden in your presentation, but you can see them if you open the speaker notes window (hit 's' on your keyboard).


===========================
## Hello There</h2>
###Introductions
<span class="fragment">Class Roster</span> 

<span class="fragment">Your turn: name, program and major, currently working with a data set? Tableau experience? what are you expecting from this class? </span> 

// story about the course what color to pick, how do we depict something, these are the numbers (show data); introductions, where you work



===========================
## Some Images</h2>
[Bad news](vizimgs/badnews.png)
[Good news](vizimgs/goodnews.png)
[Silly](http://www.politifact.com/truth-o-meter/statements/2015/oct/01/jason-chaffetz/chart-shown-planned-parenthood-hearing-misleading-/)


===========================
## Class Basics 



  
  ------------------------
  ## Expectations
  <span class="fragment">Learn how to deal with data</span><br>
  <span class="fragment">Learn to tell a data story clearly and responsibly</span><br>
  <span class="fragment">Learn practical skills</span><br>
  <span class="fragment">Learn Tableau</span><br>
  <span class="fragment">Have fun</span><br>

  ------------------------
  ## [Syllabus](https://blackboard.gwu.edu/bbcswebdav/pid-6894462-dt-content-rid-14491757_2/courses/67329_201503/PubH6299_12%20SyllabusFinalFall2015.pdf) 


  ------------------------
  ## Assignments and Final Project
  All 3 assignments build up to the final project, the presentation of your data story.
  1. Identify the question 
  2. Summarize your data
  3. Show relationship among variables

  
  
  ------------------------
  ## Class Format
  * Recap and questions 15 min
  * Lecture 20 min
  * Tableau 70 min
  * MP & Wrap-up 5 min

// ella end
===========================
# Recap and Questions



===========================
# Lecture



===========================
## Today
* Overview of the class
* The process of visual exploration
* Introduction to Tableau
* Wrap-up
* Minute papers

===========================
## Exploring vs. Testing
<dl>
  <dt>_Testing_</dt>
  <dd>Precise questions, binary answers</dd>
  <dt>_Exploring_</dt>
  <dd>More general questions, open ended answers</dd>
</dt>

  ------------------------
  ### The Testing Approach
  * Pre-specified hypotheses
  * Formal methods, test statistics, p-values
  * Binary decisions: 

  Goal: binary decision, accept/reject

  ------------------------
  ### The Exploratory Approach
  * Pre-specified _vague_ hypotheses
  * Mixture of formal and informal methods
  * Visual tests, permutation p-values

  Goal: better understand the hypotheses

  ------------------------
  ### In practice
  * Mixture of testing and exploration
  * e.g. Clinical Trials
     * Primary and secondary hypotheses
     * Other hypotheses

  ------------------------
  ### Type I and Type II errors
  <dl>
    <dt>_Type I error_</dt>
    <dd>False positive, mistaking a fluke in the sample for something real in the population</dd>
    <dt>_Type II error_</dt>
    <dd>False negative, missing a real effect because it's weak or in the sample, or because you didn't pick it up</dd>
  </dt>

  ------------------------
  ### Type I and Type II errors
  Can occur any time to reach a conclusion<br>
  <span class='fragment'>even if it involves no formal testing</span><br>
  <span class='fragment'>even if you're "just" exploring</span><br>

===========================
## Exploring vs. Communicating
* Part of a continuum
* Related but different skills

  ------------------------
  ### Communicating results
  * Involves other people
  * People from other disciplines <!-- .element: class="fragment" -->
  * People with different interests <!-- .element: class="fragment" -->

  ------------------------
  ### Other disciplines
  * The audience may not speak your technical language
  * Even if they do, they may not know your area <!-- .element: class="fragment" -->
  * They are not required to study your field to prepare for your presentation <!-- .element: class="fragment" -->
  
  <span class='fragment'>Making sure they understand you is _your_ responsibility</span>

  ------------------------
  ### Other interests
  * Nobody cares about what you did
  * What part of your work can they use? What part is relevant to them?<!-- .element: class="fragment" -->

  ------------------------
  ### Is your work relevant to others?
  * Did you solve a general problem?
  * Are your data representative of broader groups?
  * Do your results generalize?
  * Are your conclusions correct?

  Communicating involves answering these questions

  ------------------------
  ### Messing up
  * Your data have major errors in them
  * Your sample is highly biased
  * Your analysis is inappropriate
  * The questions you answer are too narrow
  * You can't communicate your results <!-- .element: class="fragment" -->


===========================
## The Analysis Pipeline

  ![Pipeline](vizimgs/Lec01Pipeline.png)

  -------------------------
  ### Starting from a good question
  What's a good question?
  * It's relevant <!-- .element: class="fragment" data-fragment-index="1" -->
  * It's not already answered <!-- .element: class="fragment" data-fragment-index="2" -->
  * It can be answered <!-- .element: class="fragment" data-fragment-index="3" -->

  -------------------------
  ### Operationalize the question
  * What data do you need?
  * What aspects of the data will you look at?
  * How will you look?

  -------------------------
  ### Obtain data
  * Conduct a study
  * Use existing data

  -------------------------
  ### Understand the data
  * What do the variables actually measure?
  * How were the data obtained?
  * What are the weaknesses?

  -------------------------
  ### Read the data correctly
  * Hopefully they're in a decent format
  * Or maybe they're in a Excel spreadsheet from hell <!-- .element: class="fragment" -->

  _Lecture 2_

  -------------------------
  ### Clean the data

  _Without data visualization:_<br>
  Garbage in, garbage out
  
  _Without data visualization:_<br>
  Garbage in, good looking garbage out

  _Lecture 2_

  -------------------------
  ### Look for answers
  * Visual methods
  * Appropriate summaries

  _Lectures 3-5_

  -------------------------
  ### Avoid type I and type II errors
  * Finding flukes
  * Missing important things
  * What's your power? p-value?

  _Lecture 5_

  -------------------------
  ### Express what you found
  * Graphics, tables, other
  * Are they clear to your audience?
  * Do they say what you want them to say?

  _Lectures 3-6_

  -------------------------
  ### Communicate effectively
  * Tell a good story
  * Be honest

  _Lecture 6_

===========================
## Statistical Visualization
* Analysis technique, not art
* Practical goals
    * Help uncover patterns
    * Help communicate them

  -------------------------
  ### Visual tools

  * Tableau
  * Other graphical packages (_e.g. R_)
